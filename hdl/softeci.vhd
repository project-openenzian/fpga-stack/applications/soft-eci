-------------------------------------------------------------------------------
-- Module Name: Enzian App - softeci - Behavioral
-------------------------------------------------------------------------------
-- Copyright (c) 2022 ETH Zurich.
-- All rights reserved.
--
-- This file is distributed under the terms in the attached LICENSE file.
-- If you do not find this file, copies can be found by writing to:
-- ETH Zurich D-INFK, Stampfenbachstrasse 114, CH-8092 Zurich. Attn: Systems Group
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library UNISIM;
use UNISIM.vcomponents.all;

library xpm;
use xpm.vcomponents.all;

use work.eci_defs.all;

entity softeci is
generic (
    FILTER_OUT_GSYNC_GINV   : boolean := true -- change it to false if you want to receive all messages including GSYNC and GINV
);
port (
-- 322.265625 MHz
    clk_sys                 : in std_logic;
-- programmed to 100MHz
    prgc0_clk_p             : in std_logic;
    prgc0_clk_n             : in std_logic;
-- programmed to 300MHz
    prgc1_clk_p             : in std_logic;
    prgc1_clk_n             : in std_logic;

-- power on reset
    reset_sys               : in std_logic;
-- ECI link status
    link1_up                : in std_logic;
    link2_up                : in std_logic;
-- ECI links
    link1_in_data           : in std_logic_vector(447 downto 0);
    link1_in_vc_no          : in std_logic_vector(27 downto 0);
    link1_in_we2            : in std_logic_vector(6 downto 0);
    link1_in_we3            : in std_logic_vector(6 downto 0);
    link1_in_we4            : in std_logic_vector(6 downto 0);
    link1_in_we5            : in std_logic_vector(6 downto 0);
    link1_in_valid          : in std_logic;
    link1_in_credit_return  : out std_logic_vector(12 downto 2);

    link1_out_hi_data       : out std_logic_vector(575 downto 0);
    link1_out_hi_vc_no      : out std_logic_vector(3 downto 0);
    link1_out_hi_size       : out std_logic_vector(2 downto 0);
    link1_out_hi_valid      : out std_logic;
    link1_out_hi_ready      : in std_logic;

    link1_out_lo_data       : out std_logic_vector(63 downto 0);
    link1_out_lo_vc_no      : out std_logic_vector(3 downto 0);
    link1_out_lo_valid      : out std_logic;
    link1_out_lo_ready      : in std_logic;
    link1_out_credit_return : in std_logic_vector(12 downto 2);

    link2_in_data           : in std_logic_vector(447 downto 0);
    link2_in_vc_no          : in std_logic_vector(27 downto 0);
    link2_in_we2            : in std_logic_vector(6 downto 0);
    link2_in_we3            : in std_logic_vector(6 downto 0);
    link2_in_we4            : in std_logic_vector(6 downto 0);
    link2_in_we5            : in std_logic_vector(6 downto 0);
    link2_in_valid          : in std_logic;
    link2_in_credit_return  : out std_logic_vector(12 downto 2);

    link2_out_hi_data       : out std_logic_vector(575 downto 0);
    link2_out_hi_vc_no      : out std_logic_vector(3 downto 0);
    link2_out_hi_size       : out std_logic_vector(2 downto 0);
    link2_out_hi_valid      : out std_logic;
    link2_out_hi_ready      : in std_logic;

    link2_out_lo_data       : out std_logic_vector(63 downto 0);
    link2_out_lo_vc_no      : out std_logic_vector(3 downto 0);
    link2_out_lo_valid      : out std_logic;
    link2_out_lo_ready      : in std_logic;
    link2_out_credit_return : in std_logic_vector(12 downto 2);

    disable_2nd_link        : out std_logic;
-- AXI Lite FPGA -> CPU
    m_io_axil_awaddr        : out std_logic_vector(43 downto 0);
    m_io_axil_awvalid       : out std_logic;
    m_io_axil_awready       : in std_logic;
    m_io_axil_wdata         : out std_logic_vector(63 downto 0);
    m_io_axil_wstrb         : out std_logic_vector(7 downto 0);
    m_io_axil_wvalid        : out std_logic;
    m_io_axil_wready        : in std_logic;
    m_io_axil_bresp         : in std_logic_vector(1 downto 0);
    m_io_axil_bvalid        : in std_logic;
    m_io_axil_bready        : out std_logic;
    m_io_axil_araddr        : out std_logic_vector(43 downto 0);
    m_io_axil_arvalid       : out std_logic;
    m_io_axil_arready       : in  std_logic;
    m_io_axil_rdata         : in std_logic_vector(63 downto 0);
    m_io_axil_rresp         : in std_logic_vector(1 downto 0);
    m_io_axil_rvalid        : in std_logic;
    m_io_axil_rready        : out std_logic;
-- AXI Lite CPU -> FPGA
    s_io_axil_awaddr        : in std_logic_vector(43 downto 0);
    s_io_axil_awvalid       : in std_logic;
    s_io_axil_awready       : out std_logic;
    s_io_axil_wdata         : in std_logic_vector(63 downto 0);
    s_io_axil_wstrb         : in std_logic_vector(7 downto 0);
    s_io_axil_wvalid        : in std_logic;
    s_io_axil_wready        : out std_logic;
    s_io_axil_bresp         : out std_logic_vector(1 downto 0);
    s_io_axil_bvalid        : out std_logic;
    s_io_axil_bready        : in std_logic;
    s_io_axil_araddr        : in std_logic_vector(43 downto 0);
    s_io_axil_arvalid       : in std_logic;
    s_io_axil_arready       : out  std_logic;
    s_io_axil_rdata         : out std_logic_vector(63 downto 0);
    s_io_axil_rresp         : out std_logic_vector(1 downto 0);
    s_io_axil_rvalid        : out std_logic;
    s_io_axil_rready        : in std_logic;
-- BSCAN slave port for ILAs, VIOs, MIGs, MDMs etc.
    s_bscan_bscanid_en      : in std_logic;
    s_bscan_capture         : in std_logic;
    s_bscan_drck            : in std_logic;
    s_bscan_reset           : in std_logic;
    s_bscan_runtest         : in std_logic;
    s_bscan_sel             : in std_logic;
    s_bscan_shift           : in std_logic;
    s_bscan_tck             : in std_logic;
    s_bscan_tdi             : in std_logic;
    s_bscan_tdo             : out std_logic;
    s_bscan_tms             : in std_logic;
    s_bscan_update          : in std_logic;
-- Microblaze Debug Module port
    mdm_SYS_Rst             : in std_logic;
    mdm_Clk                 : in std_logic;
    mdm_TDI                 : in std_logic;
    mdm_TDO                 : out std_logic;
    mdm_Reg_En              : in std_logic_vector(0 to 7);
    mdm_Capture             : in std_logic;
    mdm_Shift               : in std_logic;
    mdm_Update              : in std_logic;
    mdm_Rst                 : in std_logic;
    mdm_Disable             : in std_logic;
-- general purpose registers, accessible through the I/O space
    gpo_reg0            : in std_logic_vector(63 downto 0);
    gpo_reg1            : in std_logic_vector(63 downto 0);
    gpo_reg2            : in std_logic_vector(63 downto 0);
    gpo_reg3            : in std_logic_vector(63 downto 0);
    gpo_reg4            : in std_logic_vector(63 downto 0);
    gpo_reg5            : in std_logic_vector(63 downto 0);
    gpo_reg6            : in std_logic_vector(63 downto 0);
    gpo_reg7            : in std_logic_vector(63 downto 0);
    gpo_reg8            : in std_logic_vector(63 downto 0);
    gpo_reg9            : in std_logic_vector(63 downto 0);
    gpo_reg10           : in std_logic_vector(63 downto 0);
    gpo_reg11           : in std_logic_vector(63 downto 0);
    gpo_reg12           : in std_logic_vector(63 downto 0);
    gpo_reg13           : in std_logic_vector(63 downto 0);
    gpo_reg14           : in std_logic_vector(63 downto 0);
    gpo_reg15           : in std_logic_vector(63 downto 0);
    gpi_reg0            : out std_logic_vector(63 downto 0);
    gpi_reg1            : out std_logic_vector(63 downto 0);
    gpi_reg2            : out std_logic_vector(63 downto 0);
    gpi_reg3            : out std_logic_vector(63 downto 0);
    gpi_reg4            : out std_logic_vector(63 downto 0);
    gpi_reg5            : out std_logic_vector(63 downto 0);
    gpi_reg6            : out std_logic_vector(63 downto 0);
    gpi_reg7            : out std_logic_vector(63 downto 0);
    gpi_reg8            : out std_logic_vector(63 downto 0);
    gpi_reg9            : out std_logic_vector(63 downto 0);
    gpi_reg10           : out std_logic_vector(63 downto 0);
    gpi_reg11           : out std_logic_vector(63 downto 0);
    gpi_reg12           : out std_logic_vector(63 downto 0);
    gpi_reg13           : out std_logic_vector(63 downto 0);
    gpi_reg14           : out std_logic_vector(63 downto 0);
    gpi_reg15           : out std_logic_vector(63 downto 0);
-- DDR4
    F_D1_ACT_N : out std_logic;
    F_D1_A : out std_logic_vector ( 17 downto 0 );
    F_D1_BA : out std_logic_vector ( 1 downto 0 );
    F_D1_BG : out std_logic_vector ( 1 downto 0 );
    F_D1_CK_N : out std_logic_vector ( 1 downto 0 );
    F_D1_CK_P : out std_logic_vector ( 1 downto 0 );
    F_D1_CKE : out std_logic_vector ( 1 downto 0 );
    F_D1_CS_N : out std_logic_vector ( 3 downto 0 );
    F_D1_DQ : inout std_logic_vector ( 71 downto 0 );
    F_D1_DQS_N : inout std_logic_vector ( 17 downto 0 );
    F_D1_DQS_P : inout std_logic_vector ( 17 downto 0 );
    F_D1_ODT : out std_logic_vector ( 1 downto 0 );
    F_D1_PARITY_N : out std_logic;
    F_D1_RESET_N : out std_logic;
    F_D1C_CLK_N : in std_logic;
    F_D1C_CLK_P : in std_logic;

    F_D2_ACT_N : out std_logic;
    F_D2_A : out std_logic_vector ( 17 downto 0 );
    F_D2_BA : out std_logic_vector ( 1 downto 0 );
    F_D2_BG : out std_logic_vector ( 1 downto 0 );
    F_D2_CK_N : out std_logic_vector ( 1 downto 0 );
    F_D2_CK_P : out std_logic_vector ( 1 downto 0 );
    F_D2_CKE : out std_logic_vector ( 1 downto 0 );
    F_D2_CS_N : out std_logic_vector ( 3 downto 0 );
    F_D2_DQ : inout std_logic_vector ( 71 downto 0 );
    F_D2_DQS_N : inout std_logic_vector ( 17 downto 0 );
    F_D2_DQS_P : inout std_logic_vector ( 17 downto 0 );
    F_D2_ODT : out std_logic_vector ( 1 downto 0 );
    F_D2_PARITY_N : out std_logic;
    F_D2_RESET_N : out std_logic;
    F_D2C_CLK_N : in std_logic;
    F_D2C_CLK_P : in std_logic;

    F_D3_ACT_N : out std_logic;
    F_D3_A : out std_logic_vector ( 17 downto 0 );
    F_D3_BA : out std_logic_vector ( 1 downto 0 );
    F_D3_BG : out std_logic_vector ( 1 downto 0 );
    F_D3_CK_N : out std_logic_vector ( 1 downto 0 );
    F_D3_CK_P : out std_logic_vector ( 1 downto 0 );
    F_D3_CKE : out std_logic_vector ( 1 downto 0 );
    F_D3_CS_N : out std_logic_vector ( 3 downto 0 );
    F_D3_DQ : inout std_logic_vector ( 71 downto 0 );
    F_D3_DQS_N : inout std_logic_vector ( 17 downto 0 );
    F_D3_DQS_P : inout std_logic_vector ( 17 downto 0 );
    F_D3_ODT : out std_logic_vector ( 1 downto 0 );
    F_D3_PARITY_N : out std_logic;
    F_D3_RESET_N : out std_logic;
    F_D3C_CLK_N : in std_logic;
    F_D3C_CLK_P : in std_logic;

    F_D4_ACT_N : out std_logic;
    F_D4_A : out std_logic_vector ( 17 downto 0 );
    F_D4_BA : out std_logic_vector ( 1 downto 0 );
    F_D4_BG : out std_logic_vector ( 1 downto 0 );
    F_D4_CK_N : out std_logic_vector ( 1 downto 0 );
    F_D4_CK_P : out std_logic_vector ( 1 downto 0 );
    F_D4_CKE : out std_logic_vector ( 1 downto 0 );
    F_D4_CS_N : out std_logic_vector ( 3 downto 0 );
    F_D4_DQ : inout std_logic_vector ( 71 downto 0 );
    F_D4_DQS_N : inout std_logic_vector ( 17 downto 0 );
    F_D4_DQS_P : inout std_logic_vector ( 17 downto 0 );
    F_D4_ODT : out std_logic_vector ( 1 downto 0 );
    F_D4_PARITY_N : out std_logic;
    F_D4_RESET_N : out std_logic;
    F_D4C_CLK_N : in std_logic;
    F_D4C_CLK_P : in std_logic;
-- CMAC
    F_MAC0C_CLK_P   : in std_logic;
    F_MAC0C_CLK_N   : in std_logic;
    F_MAC0_TX_P : out std_logic_vector(3 downto 0);
    F_MAC0_TX_N : out std_logic_vector(3 downto 0);
    F_MAC0_RX_P : in std_logic_vector(3 downto 0);
    F_MAC0_RX_N : in std_logic_vector(3 downto 0);

    F_MAC1C_CLK_P   : in std_logic;
    F_MAC1C_CLK_N   : in std_logic;
    F_MAC1_TX_P : out std_logic_vector(3 downto 0);
    F_MAC1_TX_N : out std_logic_vector(3 downto 0);
    F_MAC1_RX_P : in std_logic_vector(3 downto 0);
    F_MAC1_RX_N : in std_logic_vector(3 downto 0);

    F_MAC2C_CLK_P   : in std_logic;
    F_MAC2C_CLK_N   : in std_logic;
    F_MAC2_TX_P : out std_logic_vector(3 downto 0);
    F_MAC2_TX_N : out std_logic_vector(3 downto 0);
    F_MAC2_RX_P : in std_logic_vector(3 downto 0);
    F_MAC2_RX_N : in std_logic_vector(3 downto 0);

    F_MAC3C_CLK_P   : in std_logic;
    F_MAC3C_CLK_N   : in std_logic;
    F_MAC3_TX_P : out std_logic_vector(3 downto 0);
    F_MAC3_TX_N : out std_logic_vector(3 downto 0);
    F_MAC3_RX_P : in std_logic_vector(3 downto 0);
    F_MAC3_RX_N : in std_logic_vector(3 downto 0);
-- PCIE x16
    F_PCIE16C_CLK_P   : in std_logic;
    F_PCIE16C_CLK_N   : in std_logic;
    F_PCIE16_TX_P : out std_logic_vector(15 downto 0);
    F_PCIE16_TX_N : out std_logic_vector(15 downto 0);
    F_PCIE16_RX_P : in std_logic_vector(15 downto 0);
    F_PCIE16_RX_N : in std_logic_vector(15 downto 0);
-- NVMe
    F_NVMEC_CLK_P   : in std_logic;
    F_NVMEC_CLK_N   : in std_logic;
    F_NVME_TX_P : out std_logic_vector(3 downto 0);
    F_NVME_TX_N : out std_logic_vector(3 downto 0);
    F_NVME_RX_P : in std_logic_vector(3 downto 0);
    F_NVME_RX_N : in std_logic_vector(3 downto 0);
-- C2C
    B_C2CC_CLK_P    : in std_logic;
    B_C2CC_CLK_N    : in std_logic;
    B_C2C_TX_P      : in std_logic_vector(0 downto 0);
    B_C2C_TX_N      : in std_logic_vector(0 downto 0);
    B_C2C_RX_P      : out std_logic_vector(0 downto 0);
    B_C2C_RX_N      : out std_logic_vector(0 downto 0);
    B_C2C_NMI       : in std_logic;
-- I2C
    F_I2C0_SDA      : inout std_logic;
    F_I2C0_SCL      : inout std_logic;

    F_I2C1_SDA      : inout std_logic;
    F_I2C1_SCL      : inout std_logic;

    F_I2C2_SDA      : inout std_logic;
    F_I2C2_SCL      : inout std_logic;

    F_I2C3_SDA      : inout std_logic;
    F_I2C3_SCL      : inout std_logic;

    F_I2C4_SDA      : inout std_logic;
    F_I2C4_SCL      : inout std_logic;

    F_I2C5_SDA      : inout std_logic;
    F_I2C5_SCL      : inout std_logic;
    F_I2C5_RESET_N  : out std_logic;
    F_I2C5_INT_N    : in std_logic;
-- FUART
    B_FUART_TXD     : in std_logic;
    B_FUART_RXD     : out std_logic;
    B_FUART_RTS     : in std_logic;
    B_FUART_CTS     : out std_logic;
-- IRQ
    F_IRQ_IRQ0      : out std_logic;
    F_IRQ_IRQ1      : out std_logic;
    F_IRQ_IRQ2      : out std_logic;
    F_IRQ_IRQ3      : out std_logic
);
end softeci;

architecture Behavioral of softeci is

component eci_gateway is
generic (
    RX_CROSSBAR_TYPE    : string;
    DISABLE_2ND_LINK    : boolean;
    ENABLE_SOFT_ECI     : boolean;
    TX_NO_CHANNELS      : integer;
    RX_NO_CHANNELS      : integer;
    RX_FILTER_VC        : VC_BITFIELDS;
    RX_FILTER_TYPE_MASK : ECI_TYPE_MASKS;
    RX_FILTER_TYPE      : ECI_TYPE_MASKS;
    RX_FILTER_CLI_MASK  : CLI_ARRAY;
    RX_FILTER_CLI       : CLI_ARRAY
);
port (
    clk_sys                 : in std_logic;
    clk_io_out              : out std_logic;
    clk_prgc0_out           : out std_logic;
    clk_prgc1_out           : out std_logic;

    prgc0_clk_p             : in std_logic;
    prgc0_clk_n             : in std_logic;
    prgc1_clk_p             : in std_logic;
    prgc1_clk_n             : in std_logic;

    reset_sys               : in std_logic;
    reset_out               : out std_logic;
    reset_n_out             : out std_logic;
    link1_up                : in std_logic;
    link2_up                : in std_logic;

    link1_in_data           : in std_logic_vector(447 downto 0);
    link1_in_vc_no          : in std_logic_vector(27 downto 0);
    link1_in_we2            : in std_logic_vector(6 downto 0);
    link1_in_we3            : in std_logic_vector(6 downto 0);
    link1_in_we4            : in std_logic_vector(6 downto 0);
    link1_in_we5            : in std_logic_vector(6 downto 0);
    link1_in_valid          : in std_logic;
    link1_in_credit_return  : out std_logic_vector(12 downto 2);

    link1_out_hi_data       : out std_logic_vector(575 downto 0);
    link1_out_hi_vc_no      : out std_logic_vector(3 downto 0);
    link1_out_hi_size       : out std_logic_vector(2 downto 0);
    link1_out_hi_valid      : out std_logic;
    link1_out_hi_ready      : in std_logic;

    link1_out_lo_data       : out std_logic_vector(63 downto 0);
    link1_out_lo_vc_no      : out std_logic_vector(3 downto 0);
    link1_out_lo_valid      : out std_logic;
    link1_out_lo_ready      : in std_logic;
    link1_out_credit_return : in std_logic_vector(12 downto 2);

    link2_in_data           : in std_logic_vector(447 downto 0);
    link2_in_vc_no          : in std_logic_vector(27 downto 0);
    link2_in_we2            : in std_logic_vector(6 downto 0);
    link2_in_we3            : in std_logic_vector(6 downto 0);
    link2_in_we4            : in std_logic_vector(6 downto 0);
    link2_in_we5            : in std_logic_vector(6 downto 0);
    link2_in_valid          : in std_logic;
    link2_in_credit_return  : out std_logic_vector(12 downto 2);

    link2_out_hi_data       : out std_logic_vector(575 downto 0);
    link2_out_hi_vc_no      : out std_logic_vector(3 downto 0);
    link2_out_hi_size       : out std_logic_vector(2 downto 0);
    link2_out_hi_valid      : out std_logic;
    link2_out_hi_ready      : in std_logic;

    link2_out_lo_data       : out std_logic_vector(63 downto 0);
    link2_out_lo_vc_no      : out std_logic_vector(3 downto 0);
    link2_out_lo_valid      : out std_logic;
    link2_out_lo_ready      : in std_logic;
    link2_out_credit_return : in std_logic_vector(12 downto 2);

    s_bscan_bscanid_en      : in std_logic;
    s_bscan_capture         : in std_logic;
    s_bscan_drck            : in std_logic;
    s_bscan_reset           : in std_logic;
    s_bscan_runtest         : in std_logic;
    s_bscan_sel             : in std_logic;
    s_bscan_shift           : in std_logic;
    s_bscan_tck             : in std_logic;
    s_bscan_tdi             : in std_logic;
    s_bscan_tdo             : out std_logic;
    s_bscan_tms             : in std_logic;
    s_bscan_update          : in std_logic;

    m0_bscan_bscanid_en     : out std_logic;
    m0_bscan_capture        : out std_logic;
    m0_bscan_drck           : out std_logic;
    m0_bscan_reset          : out std_logic;
    m0_bscan_runtest        : out std_logic;
    m0_bscan_sel            : out std_logic;
    m0_bscan_shift          : out std_logic;
    m0_bscan_tck            : out std_logic;
    m0_bscan_tdi            : out std_logic;
    m0_bscan_tdo            : in std_logic;
    m0_bscan_tms            : out std_logic;
    m0_bscan_update         : out std_logic;

    rx_eci_channels         : out ARRAY_ECI_CHANNELS(RX_NO_CHANNELS-1 downto 0);
    rx_eci_channels_ready   : in std_logic_vector(RX_NO_CHANNELS-1 downto 0);

    tx_eci_channels         : in ARRAY_ECI_CHANNELS(TX_NO_CHANNELS-1 downto 0);
    tx_eci_channels_ready   : out std_logic_vector(TX_NO_CHANNELS-1 downto 0)
);
end component;

component loopback_vc_resp_nodata is
generic (
   WORD_WIDTH : integer;
   GSDN_GSYNC_FN : integer
);
port (
    clk, reset : in std_logic;

    -- ECI Request input stream
    vc_req_i       : in  std_logic_vector(63 downto 0);
    vc_req_valid_i : in  std_logic;
    vc_req_ready_o : out std_logic;

    -- ECI Response output stream
    vc_resp_o       : out std_logic_vector(63 downto 0);
    vc_resp_valid_o : out std_logic;
    vc_resp_ready_i : in  std_logic
);
end component;

component design_1 is
port (
    S_IO_araddr : in STD_LOGIC_VECTOR ( 43 downto 0 );
    S_IO_arprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    S_IO_arready : out STD_LOGIC;
    S_IO_arvalid : in STD_LOGIC;
    S_IO_awaddr : in STD_LOGIC_VECTOR ( 43 downto 0 );
    S_IO_awprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    S_IO_awready : out STD_LOGIC;
    S_IO_awvalid : in STD_LOGIC;
    S_IO_bready : in STD_LOGIC;
    S_IO_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    S_IO_bvalid : out STD_LOGIC;
    S_IO_rdata : out STD_LOGIC_VECTOR ( 63 downto 0 );
    S_IO_rready : in STD_LOGIC;
    S_IO_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    S_IO_rvalid : out STD_LOGIC;
    S_IO_wdata : in STD_LOGIC_VECTOR ( 63 downto 0 );
    S_IO_wready : out STD_LOGIC;
    S_IO_wstrb : in STD_LOGIC_VECTOR ( 7 downto 0 );
    S_IO_wvalid : in STD_LOGIC;
    clk : in std_logic;
    eci_packets_in_tdata : in std_logic_vector ( 31 downto 0 );
    eci_packets_in_tlast : in std_logic;
    eci_packets_in_tready : out std_logic;
    eci_packets_in_tvalid : in std_logic;
    eci_packets_out_tdata : out std_logic_vector ( 31 downto 0 );
    eci_packets_out_tlast : out std_logic;
    eci_packets_out_tready : in std_logic;
    eci_packets_out_tvalid : out std_logic;
    fpga_uart_rxd : in std_logic;
    fpga_uart_txd : out std_logic;
    mdm_debug_port_capture : in std_logic;
    mdm_debug_port_clk : in std_logic;
    mdm_debug_port_disable : in std_logic;
    mdm_debug_port_reg_en : in std_logic_vector ( 0 to 7 );
    mdm_debug_port_rst : in std_logic;
    mdm_debug_port_shift : in std_logic;
    mdm_debug_port_tdi : in std_logic;
    mdm_debug_port_tdo : out std_logic;
    mdm_debug_port_update : in std_logic;
    mdm_sys_rst : in std_logic;
    reset : in std_logic;
    resetn : in std_logic
);
end component;

component eci_axis_bridge is
port (
    clk             : in std_logic;
    reset_n         : in std_logic;

    input_tdata     : in std_logic_vector(31 downto 0);
    input_tlast     : in std_logic;
    input_tready    : out std_logic;
    input_tvalid    : in std_logic;
    output          : out ECI_CHANNEL;
    output_ready    : in std_logic;
    
    input           : in ECI_CHANNEL;
    input_ready     : out std_logic;
    output_tdata    : out std_logic_vector(31 downto 0);
    output_tlast    : out std_logic;
    output_tready   : in std_logic;
    output_tvalid   : out std_logic
);
end component;

type ECI_PACKET_RX is record
    c_gsync             : ECI_CHANNEL;
    c_gsync_ready       : std_logic;
    c_ginv              : ECI_CHANNEL;
    all_packets         : ECI_CHANNEL;
    all_packets_ready   : std_logic;
end record ECI_PACKET_RX;

type ECI_PACKET_TX is record
-- VC packets inputs, from the ThunderX
-- GSYNC packets to be looped back
    c_gsdn              : ECI_CHANNEL;
    c_gsdn_ready        : std_logic;
    all_packets         : ECI_CHANNEL;
    all_packets_ready   : std_logic;
end record ECI_PACKET_TX;

type AXI_STREAM is record
    tdata : std_logic_vector ( 31 downto 0 );
    tlast : std_logic;
    tready : std_logic;
    tvalid : std_logic;
end record;

signal eci_packets_in, eci_packets_out : AXI_STREAM;

signal link_eci_packet_rx : ECI_PACKET_RX;
signal link_eci_packet_tx : ECI_PACKET_TX;

signal clk, clk_io : std_logic;
signal reset : std_logic;
signal reset_n : std_logic;
signal link_up : std_logic;

signal fpga_uart_rxd, fpga_uart_txd  : std_logic;

type BSCAN is record
    bscanid_en     : std_logic;
    capture        : std_logic;
    drck           : std_logic;
    reset          : std_logic;
    runtest        : std_logic;
    sel            : std_logic;
    shift          : std_logic;
    tck            : std_logic;
    tdi            : std_logic;
    tdo            : std_logic;
    tms            : std_logic;
    update         : std_logic;
end record BSCAN;
signal m0_bscan : BSCAN;

begin

clk <= clk_sys;
link_up <= link1_up and link2_up;
disable_2nd_link <= '1';

gateway_gsync_ginv : if FILTER_OUT_GSYNC_GINV generate
i_eci_gateway : eci_gateway
generic map (
    -- Receive ECI messages and route them to a receiver, based on the VC number, the ECI message type and ECI message cache line index
    -- If a message matches multiple filters, it's routed to the first matching receiver
    -- type of the receiving crossbar
    --  "full" - all VC paths are separate and independent, but resource utilization and latency increases
    --  "lite" - VCs are aggregated into 3 groups: (2, 3, 4, 5), (6, 8, 10, 12) and (7, 9, 11). It reduces resource utilization and latency, but doesn't permit blocking operations
    RX_CROSSBAR_TYPE    => "lite",
    ENABLE_SOFT_ECI     => true,
    DISABLE_2ND_LINK    => true,

    -- number of transmitting channels
    TX_NO_CHANNELS      => 2,
    -- number of receiving channels
    RX_NO_CHANNELS      => 3,
    -- 1. GSYNC messages
    -- 2. GINV messages
    -- 3. All other messages
    -- receiving channels VC bitmasks
    RX_FILTER_VC        => (ECI_FILTER_VC_MASK((6, 7)),
                            ECI_FILTER_VC_MASK((6, 7)),
                            ECI_FILTER_VC_MASK((2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12))),
    -- receiving channels type masks, defines which type bits matter
    RX_FILTER_TYPE_MASK => ("11111",
                            "11111",
                            ECI_FILTER_TYPE_UNUSED),
    -- receiving channels types, used to compare with the masked ECI type
    RX_FILTER_TYPE      => (ECI_MREQ_GSYNC,
                            ECI_MREQ_GINV,
                            ECI_FILTER_TYPE_UNUSED),
    RX_FILTER_CLI_MASK  => (ECI_FILTER_CLI_UNUSED,
                            ECI_FILTER_CLI_UNUSED,
                            ECI_FILTER_CLI_UNUSED),
    RX_FILTER_CLI       => (ECI_FILTER_CLI_UNUSED,
                            ECI_FILTER_CLI_UNUSED,
                            ECI_FILTER_CLI_UNUSED)
)
port map (
    clk_sys                 => clk,
    clk_io_out              => clk_io,

    prgc0_clk_p             => prgc0_clk_p,
    prgc0_clk_n             => prgc0_clk_n,
    prgc1_clk_p             => prgc1_clk_p,
    prgc1_clk_n             => prgc1_clk_n,

    reset_sys               => reset_sys,
    reset_out               => reset,
    reset_n_out             => reset_n,
    link1_up                => link1_up,
    link2_up                => link2_up,

    link1_in_data           => link1_in_data,
    link1_in_vc_no          => link1_in_vc_no,
    link1_in_we2            => link1_in_we2,
    link1_in_we3            => link1_in_we3,
    link1_in_we4            => link1_in_we4,
    link1_in_we5            => link1_in_we5,
    link1_in_valid          => link1_in_valid,
    link1_in_credit_return  => link1_in_credit_return,

    link1_out_hi_data       => link1_out_hi_data,
    link1_out_hi_vc_no      => link1_out_hi_vc_no,
    link1_out_hi_size       => link1_out_hi_size,
    link1_out_hi_valid      => link1_out_hi_valid,
    link1_out_hi_ready      => link1_out_hi_ready,

    link1_out_lo_data       => link1_out_lo_data,
    link1_out_lo_vc_no      => link1_out_lo_vc_no,
    link1_out_lo_valid      => link1_out_lo_valid,
    link1_out_lo_ready      => link1_out_lo_ready,
    link1_out_credit_return => link1_out_credit_return,

    link2_in_data           => link2_in_data,
    link2_in_vc_no          => link2_in_vc_no,
    link2_in_we2            => link2_in_we2,
    link2_in_we3            => link2_in_we3,
    link2_in_we4            => link2_in_we4,
    link2_in_we5            => link2_in_we5,
    link2_in_valid          => link2_in_valid,
    link2_in_credit_return  => link2_in_credit_return,

    link2_out_hi_data       => link2_out_hi_data,
    link2_out_hi_vc_no      => link2_out_hi_vc_no,
    link2_out_hi_size       => link2_out_hi_size,
    link2_out_hi_valid      => link2_out_hi_valid,
    link2_out_hi_ready      => link2_out_hi_ready,

    link2_out_lo_data       => link2_out_lo_data,
    link2_out_lo_vc_no      => link2_out_lo_vc_no,
    link2_out_lo_valid      => link2_out_lo_valid,
    link2_out_lo_ready      => link2_out_lo_ready,
    link2_out_credit_return => link2_out_credit_return,

    s_bscan_bscanid_en      => s_bscan_bscanid_en,
    s_bscan_capture         => s_bscan_capture,
    s_bscan_drck            => s_bscan_drck,
    s_bscan_reset           => s_bscan_reset,
    s_bscan_runtest         => s_bscan_runtest,
    s_bscan_sel             => s_bscan_sel,
    s_bscan_shift           => s_bscan_shift,
    s_bscan_tck             => s_bscan_tck,
    s_bscan_tdi             => s_bscan_tdi,
    s_bscan_tdo             => s_bscan_tdo,
    s_bscan_tms             => s_bscan_tms,
    s_bscan_update          => s_bscan_update,

    m0_bscan_bscanid_en     => m0_bscan.bscanid_en,
    m0_bscan_capture        => m0_bscan.capture,
    m0_bscan_drck           => m0_bscan.drck,
    m0_bscan_reset          => m0_bscan.reset,
    m0_bscan_runtest        => m0_bscan.runtest,
    m0_bscan_sel            => m0_bscan.sel,
    m0_bscan_shift          => m0_bscan.shift,
    m0_bscan_tck            => m0_bscan.tck,
    m0_bscan_tdi            => m0_bscan.tdi,
    m0_bscan_tdo            => m0_bscan.tdo,
    m0_bscan_tms            => m0_bscan.tms,
    m0_bscan_update         => m0_bscan.update,

    rx_eci_channels(0)      => link_eci_packet_rx.c_gsync,
    rx_eci_channels(1)      => link_eci_packet_rx.c_ginv,
    rx_eci_channels(2)      => link_eci_packet_rx.all_packets,

    rx_eci_channels_ready(0)   => link_eci_packet_rx.c_gsync_ready,
    rx_eci_channels_ready(1)   => '1',
    rx_eci_channels_ready(2)   => link_eci_packet_rx.all_packets_ready,

    tx_eci_channels(0)      => link_eci_packet_tx.c_gsdn,
    tx_eci_channels(1)      => link_eci_packet_tx.all_packets,

    tx_eci_channels_ready(0)   => link_eci_packet_tx.c_gsdn_ready,
    tx_eci_channels_ready(1)   => link_eci_packet_tx.all_packets_ready
);
end generate gateway_gsync_ginv;

gateway_all : if not FILTER_OUT_GSYNC_GINV generate
i_eci_gateway : eci_gateway
generic map (
    -- Receive ECI messages and route them to a receiver, based on the VC number, the ECI message type and ECI message cache line index
    -- If a message matches multiple filters, it's routed to the first matching receiver
    -- type of the receiving crossbar
    --  "full" - all VC paths are separate and independent, but resource utilization and latency increases
    --  "lite" - VCs are aggregated into 3 groups: (2, 3, 4, 5), (6, 8, 10, 12) and (7, 9, 11). It reduces resource utilization and latency, but doesn't permit blocking operations
    RX_CROSSBAR_TYPE    => "lite",
    ENABLE_SOFT_ECI     => true,
    DISABLE_2ND_LINK    => true,

    -- number of transmitting channels
    TX_NO_CHANNELS      => 2,
    -- number of receiving channels
    RX_NO_CHANNELS      => 3,
    -- 1. All messages
    -- receiving channels type masks, defines which type bits matter
    RX_FILTER_VC        => (ECI_FILTER_VC_MASK((2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12)),
                            ECI_FILTER_VC_MASK((6, 7)),
                            ECI_FILTER_VC_MASK((6, 7))),
    -- receiving channels type masks, defines which type bits matter
    RX_FILTER_TYPE_MASK => (ECI_FILTER_TYPE_UNUSED,
                            "11111",
                            "11111"),
    -- receiving channels types, used to compare with the masked ECI type
    RX_FILTER_TYPE      => (ECI_FILTER_TYPE_UNUSED,
                            ECI_MREQ_GSYNC,
                            ECI_MREQ_GINV),
    RX_FILTER_CLI_MASK  => (ECI_FILTER_CLI_UNUSED,
                            ECI_FILTER_CLI_UNUSED,
                            ECI_FILTER_CLI_UNUSED),
    RX_FILTER_CLI       => (ECI_FILTER_CLI_UNUSED,
                            ECI_FILTER_CLI_UNUSED,
                            ECI_FILTER_CLI_UNUSED)
)
port map (
    clk_sys                 => clk,
    clk_io_out              => clk_io,

    prgc0_clk_p             => prgc0_clk_p,
    prgc0_clk_n             => prgc0_clk_n,
    prgc1_clk_p             => prgc1_clk_p,
    prgc1_clk_n             => prgc1_clk_n,

    reset_sys               => reset_sys,
    reset_out               => reset,
    reset_n_out             => reset_n,
    link1_up                => link1_up,
    link2_up                => link2_up,

    link1_in_data           => link1_in_data,
    link1_in_vc_no          => link1_in_vc_no,
    link1_in_we2            => link1_in_we2,
    link1_in_we3            => link1_in_we3,
    link1_in_we4            => link1_in_we4,
    link1_in_we5            => link1_in_we5,
    link1_in_valid          => link1_in_valid,
    link1_in_credit_return  => link1_in_credit_return,

    link1_out_hi_data       => link1_out_hi_data,
    link1_out_hi_vc_no      => link1_out_hi_vc_no,
    link1_out_hi_size       => link1_out_hi_size,
    link1_out_hi_valid      => link1_out_hi_valid,
    link1_out_hi_ready      => link1_out_hi_ready,

    link1_out_lo_data       => link1_out_lo_data,
    link1_out_lo_vc_no      => link1_out_lo_vc_no,
    link1_out_lo_valid      => link1_out_lo_valid,
    link1_out_lo_ready      => link1_out_lo_ready,
    link1_out_credit_return => link1_out_credit_return,

    link2_in_data           => link2_in_data,
    link2_in_vc_no          => link2_in_vc_no,
    link2_in_we2            => link2_in_we2,
    link2_in_we3            => link2_in_we3,
    link2_in_we4            => link2_in_we4,
    link2_in_we5            => link2_in_we5,
    link2_in_valid          => link2_in_valid,
    link2_in_credit_return  => link2_in_credit_return,

    link2_out_hi_data       => link2_out_hi_data,
    link2_out_hi_vc_no      => link2_out_hi_vc_no,
    link2_out_hi_size       => link2_out_hi_size,
    link2_out_hi_valid      => link2_out_hi_valid,
    link2_out_hi_ready      => link2_out_hi_ready,

    link2_out_lo_data       => link2_out_lo_data,
    link2_out_lo_vc_no      => link2_out_lo_vc_no,
    link2_out_lo_valid      => link2_out_lo_valid,
    link2_out_lo_ready      => link2_out_lo_ready,
    link2_out_credit_return => link2_out_credit_return,

    s_bscan_bscanid_en      => s_bscan_bscanid_en,
    s_bscan_capture         => s_bscan_capture,
    s_bscan_drck            => s_bscan_drck,
    s_bscan_reset           => s_bscan_reset,
    s_bscan_runtest         => s_bscan_runtest,
    s_bscan_sel             => s_bscan_sel,
    s_bscan_shift           => s_bscan_shift,
    s_bscan_tck             => s_bscan_tck,
    s_bscan_tdi             => s_bscan_tdi,
    s_bscan_tdo             => s_bscan_tdo,
    s_bscan_tms             => s_bscan_tms,
    s_bscan_update          => s_bscan_update,

    m0_bscan_bscanid_en     => m0_bscan.bscanid_en,
    m0_bscan_capture        => m0_bscan.capture,
    m0_bscan_drck           => m0_bscan.drck,
    m0_bscan_reset          => m0_bscan.reset,
    m0_bscan_runtest        => m0_bscan.runtest,
    m0_bscan_sel            => m0_bscan.sel,
    m0_bscan_shift          => m0_bscan.shift,
    m0_bscan_tck            => m0_bscan.tck,
    m0_bscan_tdi            => m0_bscan.tdi,
    m0_bscan_tdo            => m0_bscan.tdo,
    m0_bscan_tms            => m0_bscan.tms,
    m0_bscan_update         => m0_bscan.update,

    rx_eci_channels(0)      => link_eci_packet_rx.all_packets,
    rx_eci_channels(1)      => link_eci_packet_rx.c_gsync,
    rx_eci_channels(2)      => link_eci_packet_rx.c_ginv,

    rx_eci_channels_ready(0)   => link_eci_packet_rx.all_packets_ready,
    rx_eci_channels_ready(1)   => link_eci_packet_rx.c_gsync_ready,
    rx_eci_channels_ready(2)   => '1',

    tx_eci_channels(0)      => link_eci_packet_tx.c_gsdn,
    tx_eci_channels(1)      => link_eci_packet_tx.all_packets,

    tx_eci_channels_ready(0)   => link_eci_packet_tx.c_gsdn_ready,
    tx_eci_channels_ready(1)   => link_eci_packet_tx.all_packets_ready
);
end generate gateway_all;


-- Send GSDN responses to GSYNC requests
gsync_loopback : loopback_vc_resp_nodata
generic map (
   WORD_WIDTH => 64,
   GSDN_GSYNC_FN => 1
)
port map (
    clk   => clk,
    reset => reset,

    vc_req_i       => link_eci_packet_rx.c_gsync.data(0),
    vc_req_valid_i => link_eci_packet_rx.c_gsync.valid,
    vc_req_ready_o => link_eci_packet_rx.c_gsync_ready,

    vc_resp_o       => link_eci_packet_tx.c_gsdn.data(0),
    vc_resp_valid_o => link_eci_packet_tx.c_gsdn.valid,
    vc_resp_ready_i => link_eci_packet_tx.c_gsdn_ready
);

link_eci_packet_tx.c_gsdn.vc_no(3 downto 1) <= "101";  -- VCs 10/11
link_eci_packet_tx.c_gsdn.vc_no(0) <= not link_eci_packet_tx.c_gsdn.data(0)(7); -- 10 or 11 depends on the lowest bit of the cache line index
link_eci_packet_tx.c_gsdn.size <= ECI_CHANNEL_SIZE_0L;

i_design : design_1
port map (
    S_IO_araddr     => s_io_axil_araddr,
    S_IO_arprot     => "000",
    S_IO_arready    => s_io_axil_arready,
    S_IO_arvalid    => s_io_axil_arvalid,
    S_IO_awaddr     => s_io_axil_awaddr,
    S_IO_awprot     => "000",
    S_IO_awready    => s_io_axil_awready,
    S_IO_awvalid    => s_io_axil_awvalid,
    S_IO_bready     => s_io_axil_bready,
    S_IO_bresp      => s_io_axil_bresp,
    S_IO_bvalid     => s_io_axil_bvalid,
    S_IO_rdata      => s_io_axil_rdata,
    S_IO_rready     => s_io_axil_rready,
    S_IO_rresp      => s_io_axil_rresp,
    S_IO_rvalid     => s_io_axil_rvalid,
    S_IO_wdata      => s_io_axil_wdata,
    S_IO_wready     => s_io_axil_wready,
    S_IO_wstrb      => s_io_axil_wstrb,
    S_IO_wvalid     => s_io_axil_wvalid,
    clk             => clk,
    eci_packets_in_tdata    => eci_packets_in.tdata,
    eci_packets_in_tlast    => eci_packets_in.tlast,
    eci_packets_in_tready   => eci_packets_in.tready,
    eci_packets_in_tvalid   => eci_packets_in.tvalid,
    eci_packets_out_tdata   => eci_packets_out.tdata,
    eci_packets_out_tlast   => eci_packets_out.tlast,
    eci_packets_out_tready  => eci_packets_out.tready,
    eci_packets_out_tvalid  => eci_packets_out.tvalid,
    fpga_uart_rxd   => fpga_uart_rxd,
    fpga_uart_txd   => fpga_uart_txd,
    mdm_debug_port_capture  => mdm_capture,
    mdm_debug_port_clk      => mdm_clk,
    mdm_debug_port_disable  => mdm_disable,
    mdm_debug_port_reg_en   => mdm_reg_en,
    mdm_debug_port_rst      => mdm_rst,
    mdm_debug_port_shift    => mdm_shift,
    mdm_debug_port_tdi      => mdm_tdi,
    mdm_debug_port_tdo      => mdm_tdo,
    mdm_debug_port_update   => mdm_update,
    mdm_sys_rst             => mdm_SYS_Rst,
    reset => reset,
    resetn => reset_n
);

i_eci_axis_bridge : eci_axis_bridge
port map (
    clk             => clk,
    reset_n         => link1_up,

    input_tdata     => eci_packets_out.tdata,
    input_tlast     => eci_packets_out.tlast,
    input_tready    => eci_packets_out.tready,
    input_tvalid    => eci_packets_out.tvalid,
    output          => link_eci_packet_tx.all_packets,
    output_ready    => link_eci_packet_tx.all_packets_ready,
    
    input           => link_eci_packet_rx.all_packets,
    input_ready     => link_eci_packet_rx.all_packets_ready,
    output_tdata    => eci_packets_in.tdata,
    output_tlast    => eci_packets_in.tlast,
    output_tready   => eci_packets_in.tready,
    output_tvalid   => eci_packets_in.tvalid
);

i_fuart_txd_buf : IBUF
port map (
    I => B_FUART_TXD,
    O => fpga_uart_rxd
);

i_fuart_rxd_buf : OBUF
port map (
    I => fpga_uart_txd,
    O => B_FUART_RXD
);

end Behavioral;
