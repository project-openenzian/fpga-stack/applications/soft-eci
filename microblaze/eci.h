#ifndef ECI_H
#define ECI_H

typedef enum {
	// 1-Byte (halfword)
	ECI_SIZE1_OFFSET0  = 0,
	ECI_SIZE1_OFFSET1  = 1,
	ECI_SIZE1_OFFSET2  = 2,
	ECI_SIZE1_OFFSET3  = 3,
	ECI_SIZE1_OFFSET4  = 4,
	ECI_SIZE1_OFFSET5  = 5,
	ECI_SIZE1_OFFSET6  = 6,
	ECI_SIZE1_OFFSET7  = 7,
	// 2-Byte (singleword)
	ECI_SIZE2_OFFSET0  = 8,
	ECI_SIZE2_OFFSET2  = 9,
	ECI_SIZE2_OFFSET4  = 10,
	ECI_SIZE2_OFFSET6  = 11,
	// 4-Byte (doubleword)
	ECI_SIZE4_OFFSET0  = 12,
	ECI_SIZE4_OFFSET4  = 13,
	// 8-Byte (quadword)
	ECI_SIZE8_OFFSET0  = 14,
	// 16-Byte (octaword)
	ECI_SIZE16_OFFSET0 = 15
} eci_szoff_t;

// VC13
typedef enum {
	ECI_MDLD_LNKD = 0x10
} eci_mdld_cmd_t;

// VC0 & 1
typedef enum {
	ECI_IREQ_IOBLD		= 0x00,
	ECI_IREQ_IOBST		= 0x02,
	ECI_IREQ_IOBSTA		= 0x03,
	ECI_IREQ_IOBSTP		= 0x04,
	ECI_IREQ_IOBSTPA	= 0x05,
	ECI_IREQ_IOBADDR	= 0x06,
	ECI_IREQ_IOBADDRA	= 0x07,
	ECI_IREQ_LMTST		= 0x08,
	ECI_IREQ_LMTSTA		= 0x09,
	ECI_IREQ_IAADD		= 0x10,
	ECI_IREQ_IACLR		= 0x12,
	ECI_IREQ_IASET		= 0x13,
	ECI_IREQ_IASWP		= 0x14,
	ECI_IREQ_IACAS		= 0x15,
	ECI_IREQ_SLILD		= 0x1c,
	ECI_IREQ_SLIST		= 0x1d,
	ECI_IREQ_IDLE		= 0x1f
} eci_ireq_cmd_t;

typedef enum {
	ECI_IRSP_IOBRSP		= 0x00,
	ECI_IRSP_IOBACK		= 0x01,
	ECI_IRSP_SLIRSP		= 0x02,
	ECI_IRSP_IDLE		= 0x1f
} eci_irsp_cmd_t;

typedef enum {
	ECI_MREQ_RLDD		= 0x00,
	ECI_MREQ_RLDI		= 0x01,
	ECI_MREQ_RLDT		= 0x02,
	ECI_MREQ_RLDY		= 0x03,
	ECI_MREQ_RLDWB		= 0x04,
	ECI_MREQ_RLDX		= 0x05,
	ECI_MREQ_RC2D_O		= 0x06,
	ECI_MREQ_RC2D_S		= 0x07,
	ECI_MREQ_RSTT		= 0x08,
	ECI_MREQ_RSTY		= 0x09,
	ECI_MREQ_RSTP		= 0x0a,
	ECI_MREQ_REOR		= 0x0b,
	ECI_MREQ_RADD		= 0x0d,
	ECI_MREQ_RINC		= 0x0e,
	ECI_MREQ_RDEC		= 0x0f,
	ECI_MREQ_RSWP		= 0x10,
	ECI_MREQ_RSET		= 0x11,
	ECI_MREQ_RCLR		= 0x12,
	ECI_MREQ_RCAS		= 0x13,
	ECI_MREQ_GINV		= 0x14,
	ECI_MREQ_RCAS_O		= 0x15,
	ECI_MREQ_RCAS_S		= 0x16,
	ECI_MREQ_RSTC		= 0x17,
	ECI_MREQ_GSYNC		= 0x18,
	ECI_MREQ_RSTC_O		= 0x19,
	ECI_MREQ_RSTC_S		= 0x1a,
	ECI_MREQ_RSMAX		= 0x1b,
	ECI_MREQ_RSMIN		= 0x1c,
	ECI_MREQ_RUMAX		= 0x1d,
	ECI_MREQ_RUMIN		= 0x1e,
	ECI_MREQ_IDLE		= 0x1f
} eci_mreq_cmd_t;

typedef enum {
	ECI_MRSP_VICD		= 0x00,
	ECI_MRSP_VICC		= 0x01,
	ECI_MRSP_VICS		= 0x02,
	ECI_MRSP_VICDHI		= 0x03,
	ECI_MRSP_HAKD		= 0x04,
	ECI_MRSP_HAKN_S		= 0x05,
	ECI_MRSP_HAKI		= 0x06,
	ECI_MRSP_HAKS		= 0x07,
	ECI_MRSP_HAKV		= 0x08,
	ECI_MRSP_PSHA		= 0x09,
	ECI_MRSP_PEMD		= 0x0a,
	ECI_MRSP_PATM		= 0x0b,
	ECI_MRSP_PACK		= 0x0c,
	ECI_MRSP_P2DF		= 0x0d,
	ECI_MRSP_GSDN 		= 0x18,
	ECI_MRSP_IDLE		= 0x1f
} eci_mrsp_cmd_t;

typedef enum {
	ECI_MFWD_FLDRO_E	= 0x00,
	ECI_MFWD_FLDRO_O	= 0x01,
	ECI_MFWD_FLDRS_E	= 0x02,
	ECI_MFWD_FLDRS_O	= 0x03,
	ECI_MFWD_FLDRS_EH	= 0x04,
	ECI_MFWD_FLDRS_OH	= 0x05,
	ECI_MFWD_FLDT_E		= 0x06,
	ECI_MFWD_FLDX_E		= 0x07,
	ECI_MFWD_FLDX_O		= 0x08,
	ECI_MFWD_FLDX_EH	= 0x09,
	ECI_MFWD_FLDX_OH	= 0x0a,
	ECI_MFWD_FEVX_EH	= 0x0b,
	ECI_MFWD_FEVX_OH	= 0x0c,
	ECI_MFWD_SINV		= 0x0d,
	ECI_MFWD_SINV_H		= 0x0e,
	ECI_MFWD_IDLE		= 0x1f
} eci_mfwd_cmd_t;

typedef enum {
    ECI_CHANNEL_SIZE_0L	= 0,
    ECI_CHANNEL_SIZE_1L	= 1,
    ECI_CHANNEL_SIZE_4L = 2,
    ECI_CHANNEL_SIZE_8L = 3,
    ECI_CHANNEL_SIZE_1F = 5,
    ECI_CHANNEL_SIZE_4F = 6,
    ECI_CHANNEL_SIZE_8F = 7
} eci_channel_size;

typedef union {
	uint32_t raw;
	struct {
		unsigned char vc : 4;
		eci_channel_size size : 4;
	} fields;
} eci_axis_header;

typedef union {
	uint64_t raw;

	struct {
		unsigned			unused:3;
		uint64_t			payload:56;
		eci_mdld_cmd_t		command:5;
	} __attribute__((__packed__)) eci_mdld;

	// IOBLD SLILD
	struct {
		eci_szoff_t 			szoff:4;	// [0:+04]
		uint64_t			addr:33;	// [4:+33]
		unsigned			did:8;		// [37:+08]
		unsigned			be:1;		// [45:+01]
		unsigned			ns:1;		// [46:+01]
		unsigned			el:2;		// [47:+02]
		unsigned			flid:3;		// [49:+03]
		unsigned			ppvid:6;	// [52:+06]
		unsigned			unused0:1;	// [58:+01]
		eci_ireq_cmd_t		command:5;
	} __attribute__((__packed__)) eci_ireq;

	// IOBACK IOBRSP SLIRSP
	struct {
		unsigned			size:4;		// [0:+04]
		uint64_t			unused0:33;	// [4:+33] addr
		unsigned			unused1:8;	// [37:+08] did
		unsigned			nxm:1;		// [45:+01]
		unsigned			unused2:3;	// [46:+03] el/ns
		unsigned			flid:3;		// [49:+03]
		unsigned			ppvid:6;	// [52:+06] only field in IOBACK
		unsigned			unused3:1;	// [58:+01]
		eci_irsp_cmd_t		command:5;
	} __attribute__((__packed__)) eci_irsp;

	// RLDD/RLDI
	// RC2D_O/RC2D_S
	// RLDT/RLDY/RLDWB/RLDX
	// RSTT/RSTY
	// RSTP
	struct {
		unsigned			unused0:5;	// [0:+05]
		unsigned			fillo:2;	    // [5:+02]
		uint64_t			A:35;		// [7:+35]
		unsigned			unused1:3;	// [42:+03]
		unsigned			ns:1;		// [45:+01]
		unsigned			dmask:4;	// [46:+04]
		unsigned			RReqID:5;	// [50:+05]
		unsigned			unused2:4;	// [55:+04]
		eci_mreq_cmd_t		command:5;  // [59:+05]
	} __attribute__((__packed__)) eci_mreq_cblk;

	// HAKD/HAKN_S/HAKI/HAKS/HAKV
	struct {
		unsigned			unused0:5;	// [0:+05]
		unsigned			fillo:2;	    // [5:+02]
		uint64_t	 		A:33;       // [7:+33]
		unsigned			unused1:5;	// [40:+05]
		unsigned			ns:1;		// [45:+01]
		unsigned			dmask:4;	// [46:+04]
		unsigned			HReqID:6;	// [50:+06]
		unsigned			unused2:3;	// [56:+03]
		eci_mrsp_cmd_t		command:5;  // [59:+05]
	} __attribute__((__packed__)) eci_mrsp_hak;

	// VICD/VICC/VICS
	struct {
		unsigned			unused0:7;	// [0:+07]
		uint64_t			A:35;		// [7:+35]
		unsigned			unused1:3;	// [42:+03]
		unsigned			ns:1;		// [45:+01]
		unsigned			dmask:4;	// [46:+04]
		unsigned			unused2:9;	// [50:+09]
		eci_mrsp_cmd_t		command:5;  // [59:+05]
	} __attribute__((__packed__)) eci_mrsp_vic;

	// PEMD/PSHA/PACK/P2DF
	struct {
		unsigned			unused0:5;	// [0:+05]
		unsigned			fillo:2;		// [5:+02]
		uint64_t			A:33;		// [7:+33]
		unsigned			unused1:1;	// [40:+01]
		unsigned			dirty:4;	// [41:+04]
		unsigned			unused2:1;	// [45:+01] [ns]
		unsigned			dmask:4;	// [46:+04]	OCI_MFWD_FLDRO_E		= 0x00,
		unsigned			RReqID:5;	// [50:+05]
		unsigned			unused3:3;	// [58:+03]
		unsigned			nxm:1;		// [59:+01]
		eci_mrsp_cmd_t		command:5;
	} __attribute__((__packed__)) eci_mrsp_cblk;

	// FLDRS_EH/FLDRS_OH
	// FLDX_EH/FLDX_OH/FEVX_EH/FEVX_OH
	// SINV_H
	struct {
		unsigned			unused0:5;	// [0:+05]
		unsigned			fillo:2;	    // [5:+02]
		uint64_t			A:33;		// [7:+33]
		unsigned			unused1:2;	// [40:+02]
		unsigned			rnode:2;	// [42:+02]
		unsigned			unused2:1;	// [44:+01]
		unsigned			ns:1;		// [45:+01]
		unsigned			dmask:4;	// [46:+04]
		unsigned			HReqID:6;	// [50:+06]
		unsigned			unused3:3;	// [56:+03]
		eci_mfwd_cmd_t		command:5;  // [59:+05]
	} __attribute__((__packed__)) eci_mfwd_pair;

	// GINV/GSYNC
	struct {
		unsigned			ppvid:6;	// [0:+06]
		unsigned			unused0:1;	// [6:+01]
		uint64_t			A:33;	   	// [7:+33]
		unsigned			unused1:5;	// [40:+05]
		unsigned			ns:1;		// [45:+01]  GINV only
		unsigned			subop:4;	// [46:+04]  GINV only
		unsigned			rlfb:5;     // [50:+05]  GINV only
		unsigned			unused2:2;	// [55:+02]
		unsigned			el:2;       // [59:+02]  GINV only
		eci_mreq_cmd_t		command:5;
	} __attribute__((__packed__)) eci_mreq_bcst;

   // GSDN
	struct 		packed {
		unsigned 			ppvid:6; 	// [0:+06]  not needed, future?
		unsigned       		unused0:1;	// [6:+01]
		uint64_t			A:33;		// [7:+33]
		unsigned			unused1:19;	// [40:+19] [46=ns]
		eci_mrsp_cmd_t		command:5;
	} __attribute__((__packed__)) eci_mrsp_bcst;


} eci_message_t;

#endif
