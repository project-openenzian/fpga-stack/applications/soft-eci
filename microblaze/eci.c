#include <stdio.h>
#include <unistd.h>
#include "xil_printf.h"
#include "mb_interface.h"
#include "eci.h"
#include "xiomodule.h"

XIOModule iomodule;

// PA, CPU physical address -> IPA, cache line index
uint64_t encode_address(uint64_t address)
{
	uint64_t t;
	t = address;
	t ^= (address >> 13) & 0x00ff;
	t ^= (address >> 5) & 0x1f07;
	t ^= (address >> 2) & 0x0018;
	return t;
}

// IPA, cache line index -> PA, CPU physical address
uint64_t decode_address(uint64_t address)
{
	uint64_t t;
	t = address;
	t ^= (address >> 18) & 0x0007;
	t ^= (address >> 15) & 0x0018;
	t ^= (address >> 13) & 0x00ff;
	t ^= (address >> 5) & 0x1f07;
	t ^= (address >> 2) & 0x0018;
	return t;
}

void send_packet(unsigned int vc, uint64_t *packet, unsigned size)
{
	unsigned i;
	eci_axis_header hdr;

	vc &= 15;
	if (size == 17) {
		hdr.fields.vc = vc;
		hdr.fields.size = ECI_CHANNEL_SIZE_8F;
		putfsl(hdr.raw, 0);
		for (i = 0; i < 8; i++) {
			putfsl((unsigned int)packet[i], 0);
			putfsl((unsigned int)(packet[i] >> 32), 0);
		}
		putfsl((unsigned int)packet[i], 0);
		cputfsl((unsigned int)(packet[i] >> 32), 0);

		hdr.fields.size = ECI_CHANNEL_SIZE_8L;
		putfsl(hdr.raw, 0);
		for (i = 9; i < 16; i++) {
			putfsl((unsigned int)packet[i], 0);
			putfsl((unsigned int)(packet[i] >> 32), 0);
		}
		putfsl((unsigned int)packet[i], 0);
		cputfsl((unsigned int)(packet[i] >> 32), 0);
	} else {
		hdr.fields.vc = vc;
		hdr.fields.size = ECI_CHANNEL_SIZE_0L;
		for (i = 0; i < size; i++) {
			putfsl(hdr.raw, 0);
			putfsl((unsigned int)packet[i], 0);
			cputfsl((unsigned int)(packet[i] >> 32), 0);
		}
	}
}

void send_dummy_data(unsigned int vc, uint64_t header, unsigned int dmask, unsigned int fillo)
{
	uint64_t packet[17];
	unsigned i, size;

	packet[0] = header;
	size = 1;
	for (i = 0; i < 4; i++) { // sends words in the order according to the dmask and fillo
		if (dmask & (1 << i)) {
			uint64_t b;

			b = (i ^ fillo) << 2;
			b |= header & 0xfffffff800ULL; // take the address from the header
			packet[size] = b + 1;
			packet[size + 1] = b + 2;
			packet[size + 2] = b + 3;
			packet[size + 3] = b + 4;
			size += 4;
		}
	}
	send_packet(vc, packet, size);
}

void print_decoded_message(unsigned incoming_vc, eci_message_t message)
{
	unsigned vc;
//	return;
	vc = incoming_vc & 14;
	if (vc == 2 || vc == 6) { // memory request
		switch (message.eci_mreq_cblk.command) {
		case ECI_MREQ_RLDD:
			iprintf("RLDD");
			break;
		case ECI_MREQ_RLDT:
			iprintf("RLDT");
			break;
		case ECI_MREQ_RLDX:
			iprintf("RLDX");
			break;
		case ECI_MREQ_RC2D_S:
			iprintf("RC2D_S");
			break;
		case ECI_MREQ_RSTT:
			iprintf("RSTT");
			break;
		case ECI_MREQ_RSTP:
			iprintf("RSTP");
			break;
		default:
			iprintf("VC%d: Unknown response (%x): %016lx\n", incoming_vc, message.eci_mreq_cblk.command, message.raw);
			break;
		}
		switch (message.eci_mreq_cblk.command) {
		case ECI_MREQ_RLDD:
		case ECI_MREQ_RLDT:
		case ECI_MREQ_RLDX:
		case ECI_MREQ_RC2D_S:
		case ECI_MREQ_RSTT:
		case ECI_MREQ_RSTP:
			iprintf(": fillo1:%x  A:%lx(%lx)  ns:%d  dmask:%x  RReqID:%x\r\n", message.eci_mreq_cblk.fillo, (uint64_t)message.eci_mreq_cblk.A,
					decode_address(message.eci_mreq_cblk.A), message.eci_mreq_cblk.ns, message.eci_mreq_cblk.dmask, message.eci_mreq_cblk.RReqID);
			break;
		default:
			break;
		}
	} else if (vc == 4 || vc == 10) { // memory response
		switch (message.eci_mrsp_bcst.command) {
		case ECI_MRSP_VICD:
			iprintf("ECI_MRSP_VICD:");
			break;
		case ECI_MRSP_VICDHI:
			iprintf("ECI_MRSP_VICDHI:");
			break;
		case ECI_MRSP_VICC:
			iprintf("ECI_MRSP_VICC:");
			break;
		case ECI_MRSP_VICS:
			iprintf("ECI_MRSP_VICS:");
			break;
		case ECI_MRSP_HAKI:
			iprintf("ECI_MRSP_HAKI:");
			break;
		case ECI_MRSP_PEMD:
			iprintf("ECI_MRSP_PEMD:");
			break;
		case ECI_MRSP_PSHA:
			iprintf("ECI_MRSP_PSHA:");
			break;
		case ECI_MRSP_GSDN:
			iprintf("ECI_MRSP_GSDN:\n");
			break;
		default:
			iprintf("VC%d: Unknown response (%x): %016lx\n", incoming_vc, message.eci_mrsp_bcst.command, message.raw);
			break;
		}
		switch (message.eci_mrsp_bcst.command) {
		case ECI_MRSP_VICD:
		case ECI_MRSP_VICDHI:
		case ECI_MRSP_VICC:
		case ECI_MRSP_VICS:
			iprintf(" A:%lx(%lx)  dmask:%x  ns:%x\r\n", (uint64_t)message.eci_mrsp_vic.A, decode_address(message.eci_mrsp_vic.A), message.eci_mrsp_vic.dmask, message.eci_mrsp_vic.ns);
			break;
		case ECI_MRSP_PEMD:
		case ECI_MRSP_PSHA:
			iprintf(" fillo:%x  A:%lx(%lx)  dirty:%d  dmask:%x  RReqID:%x  nxm:%d\r\n", message.eci_mrsp_cblk.fillo, (uint64_t)message.eci_mrsp_cblk.A,
					decode_address(message.eci_mrsp_cblk.A), message.eci_mrsp_cblk.dirty, message.eci_mrsp_cblk.dmask, message.eci_mrsp_cblk.RReqID,
					message.eci_mrsp_cblk.nxm);
			break;
		case ECI_MRSP_HAKI:
			iprintf(" fillo:%x  A:%lx(%lx)  ns:%d  dmask:%x  HReqID:%x\r\n", message.eci_mrsp_hak.fillo, (uint64_t)message.eci_mrsp_hak.A,
					decode_address(message.eci_mrsp_cblk.A), message.eci_mrsp_hak.ns, message.eci_mrsp_hak.dmask, message.eci_mrsp_hak.HReqID);
			break;
		case ECI_MRSP_GSDN:
			break;
		default:
			break;
		}
	} else if (vc == 8) { // forward
		switch (message.eci_mfwd_pair.command) {
		case ECI_MFWD_FEVX_EH:
			iprintf("ECI_MFWD_FEVX_EH");
			break;
		case ECI_MFWD_FLDRS_EH:
			iprintf("ECI_MFWD_FLDRS_EH");
			break;
		case ECI_MFWD_FLDX_EH:
			iprintf("ECI_MFWD_FLDX_EH");
			break;
		case ECI_MFWD_SINV_H:
			iprintf("ECI_MFWD_SINV_H");
			break;
		default:
			iprintf("VC%d: Unknown forward (%x): %016lx\n", incoming_vc, message.eci_mfwd_pair.command, message.raw);
			break;
		}
		switch (message.eci_mfwd_pair.command) {
		case ECI_MFWD_FEVX_EH:
		case ECI_MFWD_FLDRS_EH:
		case ECI_MFWD_FLDX_EH:
		case ECI_MFWD_SINV_H:
			iprintf(": fillo2:%x  A:%lx(%lx)  rnode:%x  ns:%d  dmask:%x  hreqid:%x\r\n", message.eci_mfwd_pair.fillo, (uint64_t)message.eci_mfwd_pair.A,
					decode_address(message.eci_mfwd_pair.A), message.eci_mfwd_pair.rnode, message.eci_mfwd_pair.ns, message.eci_mfwd_pair.dmask,
					message.eci_mfwd_pair.HReqID);
			break;
		default:
			iprintf("VC%d: Unknown forward (%x): %016lx\n", incoming_vc, message.eci_mfwd_pair.command, message.raw);
			break;
		}
	}
}

static unsigned dmask_to_payload_size(unsigned dmask)
{
	unsigned i, size;

	size = 0;
	for (i = 0; i < 4; i++) {
		if (dmask & (1 << i))
			size += 4;
	}
	return size;
}

void incoming_vc2(unsigned incoming_vc, uint64_t packet)
{
	unsigned vc = incoming_vc & 1;
	static eci_message_t request[2] = {{ .raw = 0 }, { .raw = 0 }};
	static eci_message_t response;
	static uint64_t payload[2][32];
	static unsigned payload_size[2] = {0, 0};
	static unsigned payload_to_load[2] = {0, 0};

	if (request[vc].raw == 0) {
		request[vc].raw = packet;
		switch (request[vc].eci_mreq_cblk.command) {
		case ECI_MREQ_RSTT:
			if (request[vc].eci_mreq_cblk.dmask == 0) {
				print_decoded_message(incoming_vc, request[vc]);
			} else {
				payload_size[vc] = 0;
				payload_to_load[vc] = dmask_to_payload_size(request[vc].eci_mreq_cblk.dmask);
			}
			break;
		case ECI_MREQ_RSTP:
			if (request[vc].eci_mreq_cblk.dmask == 0) {
				print_decoded_message(incoming_vc, request[vc]);
			} else {
				payload_size[vc] = 0;
				payload_to_load[vc] = dmask_to_payload_size(request[vc].eci_mreq_cblk.dmask) + 2;
			}
			break;
		default:
			iprintf("VC%d: cmd:%05x %016lx\r\n", incoming_vc, request[vc].eci_mreq_cblk.command, packet);
			request[vc].raw = 0;
		}
	} else {
		payload[vc][payload_size[vc]] = packet;
		payload_size[vc]++;
		if (payload_size[vc] == payload_to_load[vc]) {
			switch (request[vc].eci_mreq_cblk.command) {
			case ECI_MREQ_RSTT:
			case ECI_MREQ_RSTP:
				unsigned response_vc = incoming_vc + 8;

				response.raw = 0;
				response.eci_mrsp_cblk.command = ECI_MRSP_PEMD;
				response.eci_mrsp_cblk.RReqID = request[vc].eci_mreq_cblk.RReqID;
				response.eci_mrsp_cblk.dmask = 0;
				response.eci_mrsp_cblk.fillo = request[vc].eci_mreq_cblk.fillo;
				response.eci_mrsp_cblk.dirty = 0;
				response.eci_mrsp_cblk.A = request[vc].eci_mreq_bcst.A;
				send_packet(response_vc, &response.raw, 1);
				print_decoded_message(incoming_vc, request[vc]);

				break;
			default:
				print_decoded_message(incoming_vc, request[vc]);
//				int i;
//				for (i = 0; i < payload_size; i++)
//					iprintf("%x: %016lx\n", i, payload[i]);
			}
			request[vc].raw = 0;
			payload_size[vc] = 0;
		}
	}
}

void incoming_vc4(unsigned incoming_vc, uint64_t packet)
{
	unsigned vc = incoming_vc & 1;
	static eci_message_t request[2] = {{ .raw = 0 }, { .raw = 0 }};
	static uint64_t payload[2][32];
	static unsigned payload_size[2] = {0, 0};
	static unsigned payload_to_load[2] = {0, 0};

	if (request[vc].raw == 0) {
		request[vc].raw = packet;
		switch (request[vc].eci_mrsp_cblk.command) {
		case ECI_MRSP_VICDHI:
		case ECI_MRSP_VICD:
		case ECI_MRSP_VICS:
			if (request[vc].eci_mrsp_vic.dmask == 0) {
				print_decoded_message(incoming_vc, request[vc]);
			} else {
				payload_size[vc] = 0;
				payload_to_load[vc] = dmask_to_payload_size(request[vc].eci_mrsp_vic.dmask);
			}
			break;
		case ECI_MRSP_PSHA:
		case ECI_MRSP_PEMD:
			if (request[vc].eci_mrsp_cblk.dmask == 0) {
				print_decoded_message(incoming_vc, request[vc]);
			} else {
				payload_size[vc] = 0;
				payload_to_load[vc] = dmask_to_payload_size(request[vc].eci_mrsp_cblk.dmask);
			}
			break;
		case ECI_MRSP_PATM:
			unsigned size = (request[vc].raw >> 42) & 1;
			payload_to_load[vc] = 1;
			if (size == 1)
				payload_to_load[vc] = 2;
			break;
		case ECI_MRSP_HAKV:
			break;
		default:
			iprintf("VC%d: cmd:%05x %016lx\r\n", incoming_vc, request[vc].eci_mrsp_bcst.command, packet);
			request[vc].raw = 0;
		}
	} else {
		payload[vc][payload_size[vc]] = packet;
		payload_size[vc]++;
		if (payload_size[vc] == payload_to_load[vc]) {
			switch (request[vc].eci_mrsp_cblk.command) {
			case ECI_MRSP_PATM:
				iprintf("PATM(%d) %016lx %016lx\n", payload_to_load[vc], payload[vc][0], payload[vc][1]);
				break;
			default:
				print_decoded_message(incoming_vc, request[vc]);
//				int i;
//				for (i = 0; i < payload_size; i++)
//					iprintf("%x: %016lx\n", i, payload[i]);
			}
			request[vc].raw = 0;
			payload_size[vc] = 0;
		}
	}
}

void incoming_vc6(unsigned incoming_vc, uint64_t packet)
{
	eci_message_t request;

	request.raw = packet;
	if (request.eci_mreq_bcst.command == ECI_MREQ_GINV) {
	} else if (request.eci_mreq_bcst.command == ECI_MREQ_GSYNC) {
		eci_message_t response;

		response.raw = 0;
		response.eci_mrsp_bcst.command = ECI_MRSP_GSDN;
		response.eci_mrsp_bcst.ppvid = request.eci_mreq_bcst.ppvid;
		response.eci_mrsp_bcst.A = request.eci_mreq_bcst.A;
		send_packet(11, &response.raw, 1);
	} else if (request.eci_mreq_cblk.command == ECI_MREQ_RLDD ||
			request.eci_mreq_cblk.command == ECI_MREQ_RLDX ||
			request.eci_mreq_cblk.command == ECI_MREQ_RLDT ||
			request.eci_mreq_cblk.command == ECI_MREQ_RC2D_S) {
		eci_message_t response;
		unsigned response_vc = incoming_vc & 5;

		response.raw = 0;
		response.eci_mrsp_cblk.command = (request.eci_mreq_cblk.command == ECI_MREQ_RLDD || request.eci_mreq_cblk.command == ECI_MREQ_RLDT) ? ECI_MRSP_PSHA: ECI_MRSP_PEMD;
		response.eci_mrsp_cblk.RReqID = request.eci_mreq_cblk.RReqID;
		response.eci_mrsp_cblk.dmask = request.eci_mreq_cblk.command != ECI_MREQ_RC2D_S ? request.eci_mreq_cblk.dmask: 0;
		response.eci_mrsp_cblk.fillo = request.eci_mreq_cblk.fillo;
		response.eci_mrsp_cblk.dirty = 0;
		response.eci_mrsp_cblk.A = request.eci_mreq_bcst.A;

		if (response.eci_mrsp_cblk.dmask == 0) {
			send_packet(response_vc + 6, &response.raw, 1); // the CPU is not interested in data
		} else {
			send_dummy_data(response_vc, response.raw, response.eci_mrsp_cblk.dmask, response.eci_mrsp_cblk.fillo);
		}
		print_decoded_message(incoming_vc, request);
	} else
		iprintf("VC%d: cmd:%05x %016lx\r\n", incoming_vc, request.eci_mreq_bcst.command, packet);
}

void incoming_vc8(unsigned incoming_vc, uint64_t packet)
{
	eci_message_t request;

	request.raw = packet;
	if (request.eci_mfwd_pair.command == ECI_MFWD_FLDRS_EH || request.eci_mfwd_pair.command == ECI_MFWD_FLDX_EH) {
		eci_message_t response;
		unsigned response_vc = incoming_vc - 4;

		response.raw = 0;
		response.eci_mrsp_hak.fillo = request.eci_mfwd_pair.fillo;
		response.eci_mrsp_hak.A = request.eci_mfwd_pair.A;
		response.eci_mrsp_hak.ns = request.eci_mfwd_pair.ns;
		response.eci_mrsp_hak.dmask = request.eci_mfwd_pair.dmask;
		response.eci_mrsp_hak.HReqID = request.eci_mfwd_pair.HReqID;
		response.eci_mrsp_hak.command = ECI_MRSP_HAKD;
		send_dummy_data(response_vc, response.raw, response.eci_mrsp_hak.dmask, response.eci_mrsp_hak.fillo);

		print_decoded_message(incoming_vc, request);
	} else if (request.eci_mfwd_pair.command == ECI_MFWD_SINV_H) {
		eci_message_t response;
		unsigned response_vc = incoming_vc + 2;

		response.raw = 0;
		response.eci_mrsp_hak.fillo = request.eci_mfwd_pair.fillo;
		response.eci_mrsp_hak.A = request.eci_mfwd_pair.A;
		response.eci_mrsp_hak.ns = request.eci_mfwd_pair.ns;
		response.eci_mrsp_hak.dmask = request.eci_mfwd_pair.dmask;
		response.eci_mrsp_hak.HReqID = request.eci_mfwd_pair.HReqID;
		response.eci_mrsp_hak.command = ECI_MRSP_HAKD;
		send_packet(response_vc, &response.raw, 1);
		print_decoded_message(incoming_vc, request);
	} else if (request.eci_mfwd_pair.command == ECI_MFWD_FEVX_EH) {
		eci_message_t response;
		unsigned response_vc = incoming_vc + 2;

		response.raw = 0;
		response.eci_mrsp_vic.A = request.eci_mfwd_pair.A;
		response.eci_mrsp_vic.ns = request.eci_mfwd_pair.ns;
		response.eci_mrsp_vic.dmask = 0;
		response.eci_mrsp_vic.command = ECI_MRSP_VICDHI;
		send_packet(response_vc, &response.raw, 1);
		print_decoded_message(incoming_vc, request);
		print_decoded_message(response_vc, response);
	} else {
		iprintf("VC%d: cmd:%05x %016lx\r\n", incoming_vc, request.eci_mfwd_pair.command, packet);
	}
}

void incoming_vc10(unsigned incoming_vc, uint64_t packet)
{
	eci_message_t request;

	request.raw = packet;
	print_decoded_message(incoming_vc, request);
}

void send_rldx(uint64_t address)
{
	eci_message_t request;

	iprintf("RLDX\n");
	request.raw = 0;
	request.eci_mreq_cblk.command = ECI_MREQ_RLDX;
	request.eci_mreq_cblk.dmask = 0xf; // all sub-cache lines
	request.eci_mreq_cblk.A = encode_address(address >> 7);
	request.eci_mreq_cblk.ns = 1; // non-secure access
	send_packet(7 ^ (request.eci_mreq_cblk.A & 1), &request.raw, 1); // test packet
}

void send_rstt(uint64_t address)
{
	eci_message_t request;
	int request_vc;

	iprintf("RSTT\n");
	request.raw = 0;
	request.eci_mreq_cblk.command = ECI_MREQ_RSTT;
	request.eci_mreq_cblk.dmask = 0xf; // all sub-cache lines
	request.eci_mreq_cblk.A = encode_address(address >> 7);
	request.eci_mreq_cblk.ns = 1; // non-secure access
	request_vc = 3 ^ (request.eci_mreq_cblk.A & 1);
	send_dummy_data(request_vc, request.raw, request.eci_mreq_cblk.dmask, 0);
}

void handle_eci(void)
{
	for (;;) {
		unsigned int vc, msr;
		unsigned int value_hi, value_lo;
		for (;;) {
			uint64_t packet;
			eci_axis_header hdr;

			ngetfsl(hdr.raw, 0);
			fsl_isinvalid(msr);
			if (msr)
				break; // C flag set, nothing received

			vc = hdr.fields.vc & 14;

			getfsl(value_lo, 0);
			getfsl(value_hi, 0);
			packet = (uint64_t)value_hi << 32 | value_lo; // combine the word
			// iprintf("current timer value: %u\n", XIOModule_GetValue(&iomodule, 0));

			if (vc == 2)
				incoming_vc2(hdr.fields.vc, packet); // request with data
			else if (vc == 4)
				incoming_vc4(hdr.fields.vc, packet); // response with data
			else if (vc == 6) {
				if ((value_hi >> 27) == ECI_MREQ_GSYNC) { // fast path, send ECI_MRSP_GSDN
					vc = 10 + (hdr.fields.vc & 1);
					send_packet(vc, &packet, 1);
				}
				incoming_vc6(hdr.fields.vc, packet); // request without data
			} else if (vc == 8)
				incoming_vc8(hdr.fields.vc, packet); // forward
			else if (vc == 10)
				incoming_vc10(hdr.fields.vc, packet); // response without data
			else if (vc == 12)
				; // skip
			else // unknown message
				iprintf("i: %d -> %08x:%08x\n\r", hdr.fields.vc, value_hi, value_lo);
		}
		if (!XIOModule_IsReceiveEmpty(STDIN_BASEADDRESS)) { // check if a key was pressed
			int key;
			key = inbyte();
			iprintf("key: %d\n", key);
			// iprintf("current timer value: %u\n", XIOModule_GetValue(&iomodule, 0));

			if (key == '1') { // send RLDX
				send_rldx(0x1000000);
			} else if (key == '2') { // send RSTT
				send_rstt(0x1000000);
			}
		}
	}
}

int main()
{
// Initialize IOModule
// Timer: 322265625 ticks / second, counts down
	XIOModule_Initialize(&iomodule, XPAR_IOMODULE_0_DEVICE_ID);
	XIOModule_Start(&iomodule);
	XIOModule_Timer_SetOptions(&iomodule, 0, XTC_AUTO_RELOAD_OPTION);
	XIOModule_SetResetValue(&iomodule, 0, 0xffffffff);
	XIOModule_Timer_Start(&iomodule, 0);

	print("Soft ECI\n\r");
	handle_eci();
	return 0;
}
