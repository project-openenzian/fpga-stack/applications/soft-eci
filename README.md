# Soft ECI
Handle ECI messages in Microblaze.
You can write your C code to test out ideas before implementing them in HDL.

## Building
1. Create a project using the TCL script and build it.
2. Generate a hardware description file, XSA.
 - Open the project from the build directory, `/opt/Xilinx/Vivado/2022.1/bin/vivado -mode tcl softeci/softeci.xpr`
 - Execute `write_hw_platform -fixed softeci.xsa`
3. Create a Vitis project, using the XSA file as a plaform definition.
4. Add the C files, `eci.c` and `eci.h` from the subdirectory `microblaze` to your Vitis project and build it.
