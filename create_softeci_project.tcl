#   Copyright (c) 2022 ETH Zurich.
#   All rights reserved.
#
#   This file is distributed under the terms in the attached LICENSE file.
#   If you do not find this file, copies can be found by writing to:
#   ETH Zurich D-INFK, Stampfenbachstrasse 114, CH-8092 Zurich. Attn: Systems Group

# Project name & top level module
set project     "softeci"
# Project source directory
set project_dir "."

set part        "xcvu9p-flgb2104-3-e"
#set board_part  "eth.ch:enzian:1.0"

# IP location must be passed as the first argument.
set ip_dir [lindex $argv 0]

# Source files are included relative to the directory containing this script.
set src_dir   [file normalize "[file dirname [info script]]/${project_dir}"]
set build_dir [file normalize "."]

# Create project
create_project $project "${build_dir}/${project}" -part $part
set proj [current_project]

# Set project properties
#set_property "board_part" $board_part                   $proj
set_property "default_lib" "xil_defaultlib"                 $proj
set_property "ip_cache_permissions" "read write"            $proj
set_property "ip_output_repo" "${build_dir}/${project}/${project}.cache/ip"  $proj
set_property "sim.ip.auto_export_scripts" "1"               $proj
set_property "simulator_language" "Mixed"                   $proj
set_property "target_language" "VHDL"                       $proj
set_property "xpm_libraries" "XPM_CDC XPM_MEMORY"           $proj
set_property "ip_repo_paths" "${ip_dir}"                      $proj
set_property -name {STEPS.SYNTH_DESIGN.ARGS.MORE OPTIONS} -value {-mode out_of_context} -objects [get_runs synth_1]

# Make sure any repository IP is visible.
update_ip_catalog

# Project sources
add_files -fileset [get_filesets sources_1] "${src_dir}/hdl"
# ECI toolkit sources if present
add_files -quiet -fileset [get_filesets sources_1] "${src_dir}/eci-toolkit/hdl"
# Constraints if present
add_files -quiet -fileset [get_filesets constrs_1] "${src_dir}/xdc/synth"
# ECI toolkit constraints if present
add_files -quiet -fileset [get_filesets constrs_1] "${src_dir}/eci-toolkit/xdc"

# Set top entity
set_property "top" "softeci" [get_filesets sources_1]

# Create a project-local constraint file to take debugging constraints that we
# don't want to propagate to the repository.
file mkdir "${build_dir}/${project}/${project}.srcs/constrs_1"
close [ open "${build_dir}/${project}/${project}.srcs/constrs_1/local.xdc" w ]

# ILA for ECI edge signals
create_ip -name ila -vendor xilinx.com -library ip \
              -module_name ila_0
set_property -dict [list \
    CONFIG.C_NUM_OF_PROBES {26} \
    CONFIG.C_DATA_DEPTH {4096} \
    CONFIG.C_EN_STRG_QUAL {0} \
    CONFIG.C_ADV_TRIGGER {false} \
    CONFIG.C_INPUT_PIPE_STAGES {0} \
    CONFIG.C_PROBE0_WIDTH {64} \
    CONFIG.C_PROBE1_WIDTH {64} \
    CONFIG.C_PROBE2_WIDTH {64} \
    CONFIG.C_PROBE3_WIDTH {64} \
    CONFIG.C_PROBE4_WIDTH {64} \
    CONFIG.C_PROBE5_WIDTH {64} \
    CONFIG.C_PROBE6_WIDTH {64} \
    CONFIG.C_PROBE7_WIDTH {64} \
    CONFIG.C_PROBE8_WIDTH {64} \
    CONFIG.C_PROBE9_WIDTH {4} \
    CONFIG.C_PROBE10_WIDTH {3} \
    CONFIG.C_PROBE11_WIDTH {1} \
    CONFIG.C_PROBE12_WIDTH {1} \
    CONFIG.C_PROBE13_WIDTH {64} \
    CONFIG.C_PROBE14_WIDTH {64} \
    CONFIG.C_PROBE15_WIDTH {64} \
    CONFIG.C_PROBE16_WIDTH {64} \
    CONFIG.C_PROBE17_WIDTH {64} \
    CONFIG.C_PROBE18_WIDTH {64} \
    CONFIG.C_PROBE19_WIDTH {64} \
    CONFIG.C_PROBE20_WIDTH {64} \
    CONFIG.C_PROBE21_WIDTH {64} \
    CONFIG.C_PROBE22_WIDTH {4} \
    CONFIG.C_PROBE23_WIDTH {3} \
    CONFIG.C_PROBE24_WIDTH {1} \
    CONFIG.C_PROBE25_WIDTH {1} \
    CONFIG.ALL_PROBE_SAME_MU {false} \
    ] [get_ips ila_0]
generate_target all [get_ips ila_0]



### Regenerate IP
puts "regenerate IPs"
# Regenerate ECI toolkit IPs if present
source -quiet "${src_dir}/eci-toolkit/create_ips.tcl"

# Regenerate block design
source "${src_dir}/bd/design_1.tcl"

close_project

# Update a local config file
set conffile [open "enzian.conf.tcl" "a"]
puts $conffile "set enzian_app \"${project}\""
puts $conffile "set project_dir \"${project_dir}\""
close $conffile
